Castellano

Esta aplicaci�n se llama cancionesPHP, consiste en un crud de canciones en el apartado canci�n
en el que puedes ver en una lista todas las que hay registradas, puedes hacer todas las operaciones
de un crud que las valida un archivo js y otro php; tambi�n puedes ver la localizaci�n de
"Ontinyent" en el contact y por �ltimo puedes borrar todas las canciones de una tirada
(ejercicio 9). En el proyect est� hasta el ejercicio 3 hecho y el PHP/9... es una copia de
seguridad que me hice porque iba a tocar muchas cosas de golpe y quer�a tenerlo a mano
para no tener que hacer un git clone y volver a empezar en caso de que fallara algo. Hay 
varios alerts para comprobar que el validate en js funciona y alomejor me he dejado algo
del proyecto de �ngel que no he tocado (alg�n print o echo), tambi�n he decidido dejar
algunos echos y prints para que la correcci�n sea m�s sencilla.

English

This application is called cancionesPHP, it's based on a crud of songs in "canci�n" where you
can see a list of every single song registered, you can do all of crud's operations which
are validated by a php file and a js file; also, you can see the location of "Ontinyent" 
in the contact part and for the last you can delete all the songs with a click (exercise 9).
In the proyect is until exercise 3 and the PHP/9... is a security copy that I did because
I wanted to have it close so I don't have to git clone and start over if something goes wrong.
There are a few alerts to check that the js validate works and maybe I've left something
that were in Angel's proyecto (any print or echo), also I've decided to leave some echos
and prints so the correction it's easier.

Valenci�

Aquesta aplici� s'anomena cancionesPHP, est� basat en un crud de can�ons en "canci�n" on hi ha
una llista de totes les can�ons registrades, pots fer totes les operacions que et permet un
crud i estan validades per un archiu php i un altre js; a m�s, pots vore la localitzaci� d'Ontinyent
en la part de contact i per a finalitzar, en la llista tens una opci� per a borrar totes les
can�ons registrades d'una tac� (ejercici 9). En el projecte est� fins l'exercici 3 i 
el PHP/9... �s una c�pia de seguretat que vaig fer perqu� volia tocar moltes coses de colp
i no volia estar fent git clone i tindre que comen�ar altra vegada. Hi queden alerts en el 
js pero a comprobar que funciona i alomillos he deixat sense volver coses del projecte d'�ngel
(echos o prints), a m�s, he decidit deixar els echos i els prints per a que la correcci�
siga m�s f�cil.