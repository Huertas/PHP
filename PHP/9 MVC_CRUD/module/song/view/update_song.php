<div id="contenido">
    <form autocomplete="on" method="post" name="aupdate_user" id="update_user" onsubmit="return validate();" action="index.php?page=controller_user&op=update">
        <h1>Modificar usuario</h1>
        <table border='0'>
            <tr>
                <td>Nombre canción: </td>
                <td><input type="text" id="cancion" name="cancion" placeholder="nombre canción" value="<?php echo $song['song'];?>" readonly/></td>
                <td><font color="red">
                    <span id="error_cancion" class="error">
                        <?php
                            echo $error['nom_cancion']
                        ?>
                    </span>
                </font></font></td>
            </tr>

            <tr>
                <td>Álbum: </td>
                <td><input type="text" id="album" name="album" placeholder="álbum" value="<?php echo $song['album'];?>"/></td>
                <td><font color="red">
                    <span id="error_album" class="error">
                        <?php
                             echo $error['album']
                        ?>
                    </span>
                </font></font></td>
            </tr>

            <tr>
                <td>Fecha de publicación: </td>
                <td><input type="text" id="fecha" name="fecha" placeholder="fecha publicacion" value="<?php echo $song['fecha_publicacion'];?>"/></td>
                <td><font color="red">
                    <span id="error_fecha_publicacion" class="error">
                        <?php
                            echo $error['fecha_publicacion']
                        ?>
                    </span>
                </font></font></td>
            </tr>

            <tr>
                <td>Genero: </td>
                <td>
                    <?php
                        if ($song['genero']==="Rock"){
                    ?>
                        <input type="radio" id="genero" name="genero" placeholder="genero" value="Rock" checked/>Rock
                        <input type="radio" id="genero" name="genero" placeholder="genero" value="Pop"/>Pop
                        <input type="radio" id="genero" name="genero" placeholder="genero" value="Otros"/>Otros
                    <?php
                  }else if($song['genero']==="Pop"){
                    ?>
                    <input type="radio" id="genero" name="genero" placeholder="genero" value="Rock"/>Rock
                    <input type="radio" id="genero" name="genero" placeholder="genero" value="Pop" checked/>Pop
                    <input type="radio" id="genero" name="genero" placeholder="genero" value="Otros"/>Otros
                    <?php
                  }else{
                    ?>
                    <input type="radio" id="genero" name="genero" placeholder="genero" value="Rock"/>Rock
                    <input type="radio" id="genero" name="genero" placeholder="genero" value="Pop"/>Pop
                    <input type="radio" id="genero" name="genero" placeholder="genero" value="Otros" checked/>Otros
                    <?php
                  }
                     ?>
                </td>
                <td><font color="red">
                    <span id="error_sexo" class="error">
                        <?php
                         echo $error['sexo']
                        ?>
                    </span>
                </font></font></td>
            </tr>

            <tr>
                <td>Idioma: </td>
                <?php
                    $lang=explode(":", $user['language']);
                ?>
                <td><select multiple size="3" id="idioma[]" name="idioma[]" placeholder="idioma">
                    <?php
                        $busca_array=in_array("Español", $lang);
                        if($busca_array){
                    ?>
                        <option value="Español" selected>Español</option>
                    <?php
                        }else{
                    ?>
                        <option value="Español">Español</option>
                    <?php
                        }
                    ?>
                    <?php
                        $busca_array=in_array("Ingles", $lang);
                        if($busca_array){
                    ?>
                        <option value="Ingles" selected>Ingles</option>
                    <?php
                        }else{
                    ?>
                        <option value="Ingles">Ingles</option>
                    <?php
                        }
                    ?>
                    <?php
                        $busca_array=in_array("Portugues", $lang);
                        if($busca_array){
                    ?>
                        <option value="Portugues" selected>Portugues</option>
                    <?php
                        }else{
                    ?>
                        <option value="Portugues">Portugues</option>
                    <?php
                        }
                    ?>
                    <?php
                        $busca_array=in_array("Frances", $lang);
                        if($busca_array){
                    ?>
                        <option value="Frances" selected>Frances</option>
                    <?php
                        }else{
                    ?>
                        <option value="Frances">Frances</option>
                    <?php
                        }
                    ?>
                    </select></td>
                <td><font color="red">
                    <span id="error_idioma" class="error">
                        <?php
                            echo $error['idioma']
                        ?>
                    </span>
                </font></font></td>
            </tr>

            <tr>
                <td>Grupo: </td>
                <td><input type="text" id="grupo" name="grupo" placeholder="grupo" value="<?php echo $song['grupo'];?>"/></td>
                <td><font color="red">
                    <span id="error_grupo" class="error">
                        <?php
                            echo $error['grupo']
                        ?>
                    </span>
                </font></font></td>
            </tr>

            <tr>
                <td>Pais: </td>
                <td><select id="pais" name="pais" placeholder="pais">
                    <?php
                        if($user['country']==="España"){
                    ?>
                        <option value="España" selected>España</option>
                        <option value="Portugal">Portugal</option>
                        <option value="Francia">Francia</option>
                    <?php
                        }elseif($user['country']==="Portugal"){
                    ?>
                        <option value="España">España</option>
                        <option value="Portugal" selected>Portugal</option>
                        <option value="Francia">Francia</option>
                    <?php
                        }else{
                    ?>
                        <option value="España">España</option>
                        <option value="Portugal">Portugal</option>
                        <option value="Francia" selected>Francia</option>
                    <?php
                        }
                    ?>
                    </select></td>
                <td><font color="red">
                    <span id="error_pais" class="error">
                        <?php
                         echo $error['idioma']
                        ?>
                    </span>
                </font></font></td>
            </tr>


            <tr>
                <td>Instrumentos: </td>
                <?php
                    $afi=explode(":", $song['inst']);
                ?>
                <td>
                    <?php
                        $busca_array=in_array("Guitarra", $afi);
                        if($busca_array){
                    ?>
                        <input type="checkbox" id= "instrumento[]" name="instrumento[]" value="Guitarra" checked/>Guitarra
                    <?php
                        }else{
                    ?>
                        <input type="checkbox" id= "instrumento[]" name="instrumento[]" value="Guitarra"/>Guitarra
                    <?php
                        }
                    ?>
                    <?php
                        $busca_array=in_array("Bajo", $afi);
                        if($busca_array){
                    ?>
                        <input type="checkbox" id= "instrumento[]" name="instrumento[]" value="Bajo" checked/>Bajo
                    <?php
                        }else{
                    ?>
                        <input type="checkbox" id= "instrumento[]" name="instrumento[]" value="Bajo"/>Bajo
                    <?php
                        }
                    ?>
                    <?php
                        $busca_array=in_array("Bateria", $afi);
                        if($busca_array){
                    ?>
                        <input type="checkbox" id= "instrumento[]" name="instrumento[]" value="Bateria" checked/>Bateria</td>
                    <?php
                        }else{
                    ?>s
                    <input type="checkbox" id= "instrumento[]" name="instrumento[]" value="Bateria"/>Bateria</td>
                    <?php
                        }
                    ?>
                </td>
                <td><font color="red">
                    <span id="error_instrumento" class="error">
                        <?php
                            echo $error['instrumento']
                        ?>
                    </span>
                </font></font></td>
            </tr>




            <tr>
                <td>Link: </td>
                <td><input type="text" id="link" name="link" placeholder="link" value="<?php echo $song['link'];?>"/></td>
                <td><font color="red">
                    <span id="error_link" class="error">
                        <?php
                             echo $error['link']
                        ?>
                    </span>
                </font></font></td>

            </tr>






            <tr>
                <td><input type="submit" name="update" id="update"/></td>
                <td align="right"><a href="index.php?page=controller_user&op=list">Volver</a></td>
            </tr>
        </table>
    </form>
</div>
