


jQuery.fn.LlenarLimpiarCampos = function(){
	this.each(function(){
		$(".nombre").attr("value","Introduzca el nombre del libro");
		$(".nombre").focus(function(){
			if($(".nombre").attr("value")=="Introduzca el nombre del libro"){
			$(".nombre").attr("value","");
			}
		});
		$(".nombre").blur(function(){
			if($(".nombre").attr("value")==""){
		   $(".nombre").attr("value","Introduzca el nombre del libro");
			}
		});

		$(".isbn").attr("value","Introduzca el isbn del libro");
		$(".isbn").focus(function(){
			if($(".isbn").attr("value")=="Introduzca el isbn del libro"){
			$(".isbn").attr("value","");
			}
		});
		$(".isbn").blur(function(){
			if($(".isbn").attr("value")==""){
		   $(".isbn").attr("value","Introduzca el isbn del libro");
			}
		});


		$(".autor").attr("value","Introduzca el nombre del autor del libro");
		$(".autor").focus(function(){
			if($(".autor").attr("value")=="Introduzca el nombre del autor del libro"){
			$(".autor").attr("value","");
			}
		});
		$(".autor").blur(function(){
			if($(".autor").attr("value")==""){
		   $(".autor").attr("value","Introduzca el nombre del autor del libro");
			}
		});


		$(".fpub").attr("value","Introduzca la fecha de publicacion del libro");
		$(".fpub").focus(function(){
			if($(".fpub").attr("value")=="Introduzca la fecha de publicacion del libro"){
			$(".fpub").attr("value","");
			}
		});
		$(".fpub").blur(function(){
			if($(".fpub").attr("value")==""){
		   $(".fpub").attr("value","Introduzca la fecha de publicacion del libro");
			}
		});


		$(".numpag").attr("value","Introduzca el numero de paginas del libro");
		$(".numpag").focus(function(){
			if($(".numpag").attr("value")=="Introduzca el numero de paginas del libro"){
			$(".numpag").attr("value","");
			}
		});
		$(".numpag").blur(function(){
			if($(".numpag").attr("value")==""){
		   $(".numpag").attr("value","Introduzca el numero de paginas del libro");
			}
		});


		$(".genero").attr("value","Introduzca el genero del libro");
		$(".genero").focus(function(){
			if($(".genero").attr("value")=="Introduzca el genero del libro"){
			$(".genero").attr("value","");
			}
		});
		$(".genero").blur(function(){
			if($(".genero").attr("value")==""){
		   $(".genero").attr("value","Introduzca el genero del libro");
			}
		});



	});
	return this;
};


		$(document).ready(function () {
			$(this).LlenarLimpiarCampos();
		    $(".boton").click(function(){
		    	$(".error").remove();
				if( $(".isbn").val() == "" || $(".isbn").val() == "Introduzca el isbn del libro"){
					$(".isbn").focus().after("<span class='error'>Introduce el isbn del libro</span>");
					return false;
				}else if( $(".nombre").val() == "" || $(".nombre").val() == "Introduzca el nombre del libro"){
					$(".nombre").focus().after("<span class='error'>Introduzca el nombre del libro</span>");
					return false;
				}else if( $(".autor").val() == "" || $(".autor").val() == "Introduzca el nombre del autor del libro"){
					$(".autor").focus().after("<span class='error'>Introduzca el nombre del autor</span>");
					return false;
				}else if( $(".fpub").val() == "" || $(".fpub").val() == "Introduzca la fecha de publicacion del libro"){
					$(".fpub").focus().after("<span class='error'>Introduzca la fecha de publicacion del libro</span>");
					return false;
				}else if( $(".numpag").val() == "" || $(".numpag").val() == "Introduzca el numero de paginas del libro"){
					$(".numpag").focus().after("<span class='error'>Introduzca el numero de paginas del libro</span>");
					return false;
				}else if( $(".genero").val() == "" || $(".genero").val() == "Introduzca el genero del libro"){
					$(".genero").focus().after("<span class='error'>Introduzca el genero del libro</span>");
					return false;
				}
			});

			//realizamos funciones para que sea más práctico nuestro formulario
			$(".isbn, .nombre, .autor, .fpub, .numpag, .genero").keyup(function(){
				if ( $(this).val() != "" ){
					$(".error").fadeOut();
					return false;
				}
			});

			$(".nombre").keyup(function(){
				if ($(this).val().length >= 2){
					$(".error").fadeOut();
					return false;
				}
			});

			$(".email").keyup(function(){
				if ( $(this).val() != "" && emailreg.test($(this).val())){
					$(".error").fadeOut();
					return false;
				}
			});

			$(".mensaje").keyup(function(){
				if ($(this).val().length >= 20){
					$(".error").fadeOut();
					return false;
				}
			});


		});
