CREATE DATABASE libros;
use libros;
CREATE TABLE libro(
    id int AUTO_INCREMENT not null PRIMARY KEY,
    isbn varchar(60),
    nombre varchar(60),
    autor varchar(60),
    fecha_publicacion varchar(200),
    num_paginas int,
    genero varchar(200)
);
