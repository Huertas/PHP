<!DOCTYPE HTML>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>Formulario con un botón especial</title>
  <link rel="stylesheet" href="estilos.css" type="text/css" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.css" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0-beta1/jquery.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.js"></script>
  <script type="text/javascript" src="jqueryUI/js/jquery-ui-1.8.20.custom.min.js"></script>
  <script type="text/javascript" src="funciones.js"></script>
</head>
<body>
<div class="formulario">
<form name="form" class="contacto" action="" method="post">
  <div><label>ISBN:</label><input type='text' name="isbn" class="isbn" value="<?php @$_POST['isbn'] ?>" ><?php echo @$error[1] ?></div>
  <div><label>Nombre:</label><input type='text' name="nombre" class="nombre" value="<?php @$_POST['nombre'] ?>" ><?php echo @$error[2] ?></div>
  <div><label>Autor:</label><input type='text'  name="autor"  class="autor" value="<?php @$_POST['autor'] ?>"><?php echo @$error[3] ?></div>
  <div><label>Fecha publicacion:</label><input type='text' name="fpub"  class="fpub" value="<?php @$_POST['fpub'] ?>"><?php echo @$error[4] ?></div>
  <div><label>Numero paginas:</label><input type='text' name="numpag"  class="numpag" value="<?php @$_POST['numpag'] ?>"><?php echo @$error[5] ?></div>
  <div><label>Genero:</label><input type='text' name="genero"  class="genero" value="<?php @$_POST['genero'] ?>"><?php echo @$error[6] ?></div>
  <input type="hidden" name="grabar" value="si" />

  <div class="demo"><input type='submit' name="boton" class="boton" value='Registrar libro'/></div>
</form>
</div>
<body>
  </html>
