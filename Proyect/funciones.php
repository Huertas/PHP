<?php
if(isset($_POST['grabar']) and $_POST['grabar'] == 'si'){
    $error = array(); // declaramos un array para almacenar los errores
    if($_POST['isbn'] == '' || $_POST['isbn'] == "Introduzca el isbn del libro"){
      $error[1] = '<span class="error">Introduce el isbn del libro</span>';
    }else if($_POST['nombre'] == '' || $_POST['nombre']== "Introduzca el nombre del libro"){
      $error[2] = '<span class="error">Introduzca el nombre del libro</span>';
    }else if($_POST['autor'] == '' || $_POST['autor'] == "Introduzca el nombre del autor del libro"){
      $error[3] = '<span class="error">Introduzca el autor del libro</span>';
    }else if($_POST['fpub'] == '' || $_POST['fpub'] == "Introduzca la fecha de publicacion del libro"){
      $error[4] = '<span class="error">Introduzca la fecha de publicacion del libro</span>';
    }else if($_POST['numpag'] == '' || $_POST['numpag'] == "Introduzca el numero de paginas del libro"){
      $error[5] = '<span class="error">Introduzca el numero de paginas del libro</span>';
    }else if($_POST['genero'] == '' || $_POST['genero'] == "Introduzca el genero del libro"){
      $error[6] = '<span class="error">Introduzca el genero del libro</span>';
    }else{
      $_POST['validate']=true;
    }
  }?>
