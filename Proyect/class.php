<?php
	class Conectar{
		public static function con(){
			$host = getenv('IP');
    		$user = "root";                     //Your Cloud 9 username
    		//$user = getenv('C9_USER');
    		$pass = "";                             //Remember, there is NO password!
    		$db = "libros";                      //Your database name you want to connect to
    		$port = 3306;                           //The port #. It is always 3306
    		$tabla="libro";

    		$conexion = mysqli_connect($host, $user, $pass, $db, $port)or die(mysql_error());
			return $conexion;
		}
		public static function close($conexion){
			mysqli_close($conexion);
		}
	}
	class libros{
		function nuevo_libro($isbn, $nombre, $autor, $fpub,
		$numpag, $genero){
			 $sql = "INSERT INTO libro(isbn, nombre, autor, fecha_publicacion, num_paginas,"
                . " genero) VALUES ('$isbn', '$nombre', '$autor', '$fpub',"
                . " '$numpag', '$genero')";

            $conexion = Conectar::con();
            $res = mysqli_query($conexion, $sql);
            Conectar::close($conexion);
			return $res;
		}
		function list_fetch_assoc_libro(){
			$cad="";
			$sql = "select * from libro";

			$conexion = Conectar::con();

			$result = mysqli_query($conexion, $sql);
			while ($row = mysqli_fetch_assoc($result)) {
				//&nbsp; es un espacio en blanco
        		$cad .= "ISBN: " . $row['isbn'] . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nombre: " . $row['nombre']. "<br>";
        		$cad .= "Autor: " . $row['autor'] . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Fecha publicacion: " . $row['fecha_publicacion']. "<br>";
						$cad .= "Numero de paginas: " . $row['num_paginas'] . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Genero: " . $row['genero']. "<br>";
    		}

            Conectar::close($conexion);
			return $cad;
		}
		function list_fetch_array_libro(){
			$cad="";
			$sql = "select * from libro";

			$conexion = Conectar::con();

			$result = mysqli_query($conexion, $sql);
			while ($row = mysqli_fetch_array($result)) {
        		$cad .= "ISBN: " . $row[1] . " Nombre: " . $row[2]. "<br>";
        		$cad .= "Autor: " . $row[3] . " Fecha publicacion: " . $row[4]. "<br>";
						$cad .= "Numero de paginas: " . $row[5] . " Genero: " . $row[6]. "<br>";
    		}

            Conectar::close($conexion);
			return $cad;
		}
	}
